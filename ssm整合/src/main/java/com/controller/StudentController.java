package com.controller;


import com.domain.Student;
import com.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping("/student")
public class StudentController {
    @Autowired
    private StudentService service;
    @RequestMapping("/addStudent.do")
    public ModelAndView addStudent(Student student){
        ModelAndView mv = new ModelAndView();
        int rows = service.addStudent(student);
        String msg = "注册失败！";
        if(rows > 0){
            msg = "注册成功！";
        }
        mv.addObject("msg",msg);
        mv.setViewName("result");
        return mv;
    }
    @RequestMapping(value = "/queryStudents.do")
    public @ResponseBody List<Student> queryStudents(){
        List<Student> list = service.queryStudents();
        return list;
    }
}
