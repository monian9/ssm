package com.dao;

import com.domain.Student;
import org.springframework.stereotype.Component;

import java.util.List;

public interface StudentDao {
    int insertStudent(Student student);
    List<Student> selectStudents();
}
