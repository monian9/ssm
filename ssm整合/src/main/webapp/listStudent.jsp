<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<script type="text/javascript" src="js/jquery-3.4.1.js"></script>
<script type="text/javascript">
    $(function (){
        getStudentInfo();
        $("#doajax").click(function (){
            getStudentInfo();
        })
    })
    function getStudentInfo(){
        $.ajax({
            url:"student/queryStudents.do",
            dataType:"json",
            success:function (resp){
                $("#stuinfo").empty();
                $.each(resp,function (i,n){
                    $("#stuinfo").append("<tr><td>"+n.id+"</td><td>"
                        +n.name+"</td><td>"
                        +n.age+"</td></tr>")
                })
            }
        })
    }
</script>
<body>
    <div align="center">
        <button id="doajax">获取学生数据</button>
        <table>
            <thead>
                <tr>
                    <td>id</td>
                    <td>姓名</td>
                    <td>年龄</td>
                </tr>
            </thead>
            <tbody id="stuinfo">

            </tbody>
        </table>
    </div>
</body>
</html>
